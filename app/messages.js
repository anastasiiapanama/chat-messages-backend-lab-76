const express = require('express');
const router = express.Router();
const fileDb = require('../fileDb');

router.get('/', (req, res) => {
    if(req.query.datetime) {
        const datetime = new Date(req.query.datetime);
        if (isNaN(datetime.getDate())) {
            return res.status(400).send({message: 'Error with Date'});
        }
        return res.send(fileDb.getItemByDate(req.query.datetime));
    }
    const messages = fileDb.getItems();
    res.send(messages);
});

router.post('/', (req, res) => {
    if(!req.body.message || !req.body.author) {
        return res.status(400).send({error: 'Data is wrong!'});
    }
    fileDb.addItem(req.body);
    res.send(req.body);
});

module.exports = router;
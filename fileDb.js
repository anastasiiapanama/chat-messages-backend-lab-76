const fs = require('fs');
const {nanoid} = require("nanoid");

const fileName = './db.json';

let data = [];

module.exports = {
    init() {
        try {
            const fileContents = fs.readFileSync(fileName);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },
    getItems() {
        if(data.length > 30) {
            return data.slice(-30);
        }
        return data;
    },
    addItem(item) {
        item.id = nanoid();
        item.datetime = new Date().toISOString();
        data.push(item);
        this.save();
    },
    getItemByDate(datetime) {
        const dateArr = [];
        for (let item of data) {
            if (item.datetime > datetime) {
                dateArr.push(item);
            }
        }
        return dateArr;
    },
    save() {
        fs.writeFileSync(fileName, JSON.stringify(data, null, 2));
    }
};